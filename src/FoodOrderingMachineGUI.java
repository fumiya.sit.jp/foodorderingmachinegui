import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

public class FoodOrderingMachineGUI {
    private JPanel root;
    private JButton gyudonButton;
    private JButton gydonWithKimuchiButton;
    private JButton gyudonWith3CheesesButton;
    private JButton gyudonWithGratedRadishButton;
    private JButton gyudonWithMayonnaiseButton;
    private JButton gydonWithRawEggButton;
    private JTextPane orderedItemsList;
    private JButton checkOutButton;
    private JButton cancelAllButton;
    private JTabbedPane tabbedPane1;
    private JScrollPane scrollPane;
    private JLabel total;
    private JButton curryRiceWithBeefButton;
    private JButton curryRice￥490Button;
    private JButton wSoftBoiledEggButton;
    private JButton curryRiceWithCheesesButton;
    private JButton wBeefSoftBoiledButton;
    private JButton curryRiceWith2xButton;
    static int totalPrice = 0;
    void order(int foodPrice, String food){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if (confirmation==0) {
            JOptionPane.showMessageDialog(null,
                    "Thank you for ordering " + food +"! It will be served as soon as possible.");
            String currentText = orderedItemsList.getText();
            orderedItemsList.setText(currentText + food + " ￥" + foodPrice + "\n\n");
            totalPrice += foodPrice;
            total.setText("￥" + totalPrice + "\n");
        }
    }
    void clear(){
        orderedItemsList.setText(null);
        totalPrice = 0;
        total.setText("￥" + totalPrice + "\n");
    }

    public FoodOrderingMachineGUI() {
        gyudonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order(400, "Gyudon");
            }
        });
        gydonWithKimuchiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order(550, "Gyudon with Kimuch");
            }
        });
        gyudonWith3CheesesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order(580, "Gyudon with 3 Cheeses");
            }
        });
        gyudonWithGratedRadishButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order(540, "Gyudon with Grated Radish");
            }
        });
        gyudonWithMayonnaiseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order(560, "Gyudon with Mayonnaise");
            }
        });
        gydonWithRawEggButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order(590, "Gyudon with Raw Egg");
            }
        });
        cancelAllButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clear();
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (totalPrice != 0) {
                    int confirmation = JOptionPane.showConfirmDialog(null,
                            "Would you like to checkout?",
                            "Checkout Confirmation",
                            JOptionPane.YES_NO_OPTION);

                    if (confirmation == 0) {
                        JOptionPane.showMessageDialog(null,
                                "Thank you. The total price is " + totalPrice + " yen.");
                        clear();
                    }
                } else {
                    JOptionPane.showMessageDialog(null,
                            "No orders yet.\n" +
                                    "Please order some items.");
                }
            }
        });
        curryRiceWithBeefButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order(720, "Curry Rice with Beef");
            }
        });
        curryRice￥490Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order(490, "Curry Rice");
            }
        });
        wSoftBoiledEggButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order(570, "Curry Rice with Soft-Boiled Egg");
            }
        });
        curryRiceWithCheesesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order(620, "Curry Rice with Cheese");
            }
        });
        wBeefSoftBoiledButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order(810, "Curry Rice with Beef & Soft-boiled Egg");
            }
        });
        curryRiceWith2xButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order(920, "Curry Rice with 2x Beef");
            }
        });
        orderedItemsList.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);
                orderedItemsList.setEditable(false);
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachineGUI");
        frame.setContentPane(new FoodOrderingMachineGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
